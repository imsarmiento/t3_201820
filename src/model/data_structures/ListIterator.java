package model.data_structures;

import java.util.Iterator;
import java.util.NoSuchElementException;

//an iterator, lanza excepcion si intento eliminar
public class ListIterator<T> implements Iterator<T> {
	
    private Node<T> actual;

    public ListIterator(Node<T> head) {
        actual = head;
    }

    public boolean hasNext() {
        return actual != null;
    }

    public void remove() {
        throw new UnsupportedOperationException();
    }

    public T next() {
        if (!hasNext()) throw new NoSuchElementException();
        T item = actual.getElement();
        actual = actual.darSiguiente(); 
        return item;
    }
}

