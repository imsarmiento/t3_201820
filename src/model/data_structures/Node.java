package model.data_structures;

public class Node<T> {
	private T element;
	private Node<T> anterior;
	private Node<T> siguiente;
	public Node(T element, Node<T> pAnterior, Node<T> pSiguiente){
		this.element = element;
		anterior = pAnterior;
		siguiente = pSiguiente;
	}
	public void cambiarAnterior(Node<T> x){
		anterior = x;
	}public void cambiarSiguiente(Node<T> x){
		siguiente = x;
	}
	public Node<T> darAnterior(){
		return anterior;
	}
	public Node<T> darSiguiente(){
		return siguiente;
	}
	public T getElement(){
		return element;
	}
}
